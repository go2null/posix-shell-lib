#!/bin/sh

# == DESCRIPTION == #

# Summary: Prints error message to STDERR
#
# Usage: Logger.LEVEL [MESSAGE]
#
# LEVEL
# - debug   - Low-level information for developers.
# - info    - Generic (useful) information about system operation.
# - warn    - A warning.
# - error   - A handleable error condition.
# - fatal   - An unhandleable error that results in a program crash. DEFAULT
# - unknown - An unknown message that should always be logged.
#
# OTHER COMMANDS:
# - Logger.print [MESSAGE]             - print message to STDERR
# - Logger.abort [EXIT_CODE] [MESSAGE] - 'fatal' plus 'exit's with EXIT_CODE
# - Logger.get_level                   - return the current log level
# - Logger.set_level LEVEL             - set the current log level to LEVEL
#
# EXIT_CODE: defaults to '1'
# MESSAGE: message to print to STDERR

# == GLOBALS  == #

## Define levels
export __LOGGER_LEVEL__
export __LOGGER_DEBUG__ __LOGGER_INFO__  __LOGGER_WARN__
export __LOGGER_ERROR__ __LOGGER_FATAL__ __LOGGER_UNKNOWN__

## Set values
readonly __LOGGER_DEBUG__=0
readonly __LOGGER_INFO__=1
readonly __LOGGER_WARN__=2
readonly __LOGGER_ERROR__=3
readonly __LOGGER_FATAL__=4
readonly __LOGGER_UNKNOWN__=5

## Default Level
: "${__LOGGER_LEVEL__:=$__LOGGER_ERROR__}"

# == FUNCTIONS == #

LoggerGetLevel() { printf "%s" "$__LOGGER_LEVEL__" "$@"; }

# $1 - level, number or string
# ACTION - set __LOGGER_LEVEL__
LoggerSetLevel() {
	case "$(printf '%s' "$1" | tr '[:upper:]' '[:lower:]')" in
		$__LOGGER_DEBUG__ |   'debug')    __LOGGER_LEVEL__="$__LOGGER_DEBUG__"   ;;
		$__LOGGER_INFO__ |    'info')     __LOGGER_LEVEL__="$__LOGGER_INFO__"    ;;
		$__LOGGER_WARN__ |    'warn')     __LOGGER_LEVEL__="$__LOGGER_WARN__"    ;;
		$__LOGGER_ERROR__ |   'error')    __LOGGER_LEVEL__="$__LOGGER_ERROR__"   ;;
		$__LOGGER_FATAL__ |   'fatal')    __LOGGER_LEVEL__="$__LOGGER_FATAL__"   ;;
		$__LOGGER_UNKNOWN__ | 'unknown')  __LOGGER_LEVEL__="$__LOGGER_UNKNOWN__" ;;
	esac
}

# $1 - optional log level
# $* - message
LoggerPrint() {
	# shellcheck disable=SC2039
	local level

	level="$(printf '%s' "$1" | tr -cd '[:digit:]')"
	if [ "$level" = "$1" ]; then
		[ "$level" -lt "$__LOGGER_LEVEL__" ] && return
		shift
	fi

	printf '%s\n' "$*" 1>&2
}

LoggerDebug()    { LoggerPrint "$__LOGGER_DEBUG__"   "DEBUG:   $*"; }
LoggerError()    { LoggerPrint "$__LOGGER_ERROR__"   "ERROR:   $*"; }
LoggerFatal()    { LoggerPrint "$__LOGGER_FATAL__"   "FATAL:   $*"; }
LoggerInfo()     { LoggerPrint "$__LOGGER_INFO__"    "INFO:    $*"; }
LoggerUnknown()  { LoggerPrint "$__LOGGER_UNKNOWN__" "UNKNOWN: $*"; }
LoggerWarn()     { LoggerPrint "$__LOGGER_WARN__"    "WARN:    $*"; }

# $1 - optional, exit code
# $* - message
# ACTION - print $* and exit with error code 1
LoggerAbort() {
	# shellcheck disable=SC2039
	local exit_code

	exit_code="$(printf '%s' "$1" | tr -cd '[:digit:]')"
	# shellcheck disable=SC2015
	[ "$exit_code" = "$1" ] && shift || exit_code=1

	LoggerFatal "$*"
	exit "$exit_code"
}

