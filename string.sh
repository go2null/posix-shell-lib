#!/bin/sh
# shellcheck disable=3043

# == FUNCTIONS == #

# $1 - self, string to search within
# $2 - string to search for
# RETURN - Exit Code 0 for TRUE, non-0 for FALSE
StringIncludes() { [ "${1#*"$2"}" = "$1" ] && return 1 || return 0;  }

# $1 - self, text to indent
# $2 - number of columns to indent; text starts at next column
# RETURN - indented text
StringIndent() { printf "%$2s" && printf '%s' "$1"; }

# $* - self, string
# RETURN - string with all NEWLINES/RETURNS replaced with SPACES
StringOneLine() { printf '%s' "$*" | tr -s '\n\r' ' '; }

# $1 - number of characters to return
# RETURN - $1 number of random alnum chars
StringRandomAlnum() {
	local pool=''
	local need=0
	local blob=''
	local size=0

	while [ "$size" -lt "$1" ]; do
		need="$(($1 - size))"
		pool="$(head '/dev/urandom' | tr -dc '[:alnum:]')"
		blob="$blob$(printf "%.${need}s" "$pool")"
		size="$(printf '%s' "$blob" | wc -m)"
	done
	printf '%s' "$blob"
}

# $@ - input arguments
# RETURN - string of args in reverse order
StringReverseArgs() {
	local words=''
	while [ "$#" -gt 0 ]; do
		words="$1 $words"
		shift
	done
	printf '%s' "${words% }"
}

# $* - space separated list of words
# RETURN - words in reverse order
StringReverseWords() {
	local text=''
	local curr_rest=''
	local next_rest=''
	local words=''

	text="$(printf '%s' "$*" | tr -s ' ')"
	text="${text# }"
	text="${text% }"

	next_rest="$text"
	while [ "$curr_rest" != "$next_rest" ]; do
		curr_rest="$next_rest"
		words="$words ${curr_rest##* }"
		next_rest="${curr_rest% *}"
		echo "$words : $next_rest"
	done
	printf '%s' "${words# }"
}

# $1 - self, string to search within
# $2 - string to search for
# RETURN - Exit Code 0 for TRUE, non-0 for FALSE
StringStartsWith() { [ "${1#"$2"}" = "$1" ] && return 1 || return 0; }

# $* - string
# RETURN - string converted to lowercase/uppercase
StringDowncase() { printf '%s' "$*" | tr '[:upper:]' '[:lower:]'; }
StringUpcase()   { printf '%s' "$*" | tr '[:lower:]' '[:upper:]'; }

# $* - string
# RETURN - string with leading and/or trailing spaces removed
StringTrim() { StringTrimRight "$(StringTrimLeft "$*")"; }
StringTrimLeft()  {
	local text="$*"
	while [ "$text" != "${text# }" ]; do text="${text# }"; done
	printf '%s' "$text"
}
StringTrimRight()  {
	local text="$*"
	while [ "$text" != "${text% }" ]; do text="${text% }"; done
	printf '%s' "$text"
}
