#!/bin/sh

# == GLOBALS == #

export __REQUIRED_SCRIPTS__

# == FUNCTIONS == #

# $1 - command
# RETURN - Exit Value 0 if true, 1 if false
CommandExists() {
	command -v "$1" >/dev/null 2>&1
}

# $1 - command
# ACTION - sources script if not previously sourced
# RETURN - Exit Value 0 for success, 1 on failure
CommandRequire() {
	# shellcheck disable=SC2039
	local path script

	path="${1%/*}"
	[ -r "$path" ] || return 1

	for script in $__REQUIRED_SCRIPTS__; do
		[ "$path" = "$script" ] && return 0
	done

	__REQUIRED_SCRIPTS__="$__REQUIRED_SCRIPTS__
	$path"

	# shellcheck disable=SC1090
	. "$path"
}

# $1 - command
# ACTION - execute command if it exists
CommandTry() {
	command_exists "$1" || return $?
	$1 "$@"
}

# $1 - command
# RETURN - command type, or Exit Value >0 if not found
CommandType() {
	# shellcheck disable=SC2039
	local message exit_value

	message="$(command -V "$1" 2>&1)"
	exit_value=$?

	[ "$exit_value" -gt 0 ] && echo 'not found' && return $exit_value

	case "$message" in
		"$1 is "*'alias'*' '*)            echo 'alias'                 ;;
		"$1 is a "*'function'*)           echo 'function'              ;;
		"$1 is a "*'word')                echo 'keyword'               ;;
		"$1 is a shell builtin")          echo 'builtin'               ;;
		"$1 is a special shell builtin")  echo 'special builtin'       ;;
		"$1 is a tracked alias for"*)     echo 'command'               ;;
		"$1 is /"*)                       echo 'command'               ;;
		*)                                echo 'UNKNOWN' && return 127 ;;
	esac
}
