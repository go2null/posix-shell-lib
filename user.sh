#!/bin/sh

# == DESCRIPTION == #

# POSIX functions to be sourced by a calling script

# == FUNCTIONS == #

UserIsRoot() { [ "$(id -u)" -eq 0  ]; }
UserIsSudo() { [ -n "$SUDO_USER" ]; }
