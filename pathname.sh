#!/bin/sh

# == DESCRIPTION == #

# POSIX functions to be sourced by a calling script
#
# This file is modelled after the Ruby Pathname class.
# Look there for details.

# == FUNCTIONS == #

# $1 - path
# ACTION - clean path
# RETURN - cleaned path. Exit Value 1 if path is empty string.
PathnameCleanpath() {
	# shellcheck disable=SC2039
	local path left right

	[ -z "$1" ] && return 1

	# remove repeated '/'
	path=$(printf '%s' "$1" | tr -s '/')
	# remove trailing '/'
	path=${path%/}

	# remove path navigation
	until [ "$path" = "${path#*/../}" ]; do
		left="${path%%/../*}"
		left="${left%/*}"
		right="${path#*/../}"
		path="$left/$right"
	done

	printf '%s' "$path"
}

# $1 - directory path
# ACTION - verify that it is an existing dorectory
# RETURN - clean path: a/..b/c -> b/c
PathnameRealpath() {
	# shellcheck disable=SC2039
	local dir path

	# verify path exists
	[ ! -e "$1" ] && return 1
	# get directory path
	[ -d "$1" ] && dir="$1" || dir="${1%/*}"
	# get absolute directory path
	path="$(cd "$dir" && pwd)"
	# append back filename is previously stripped
	[ "$dir" != "$1" ] && path="$path/${1##*/}"
	# return path
	printf '%s' "$path"
}
